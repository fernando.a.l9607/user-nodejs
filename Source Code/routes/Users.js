const express = require('express')
const users = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const User = require('../models/User')
users.use(cors())

process.env.SECRET_KEY = 'secret'

users.post('/register', (req, res) => {
    const today = new Date()
    const userData = {
        name: req.body.name,
        surnames: req.body.surnames,
        email: req.body.email,
        password: req.body.password,
        produce: today
    }

    User.findOne({
        where: {
            email: req.body.email
        }
    })
    .then(usuario => {
        if(!usuario){
            bcrypt.hash(req.body.password, 10, (err, bcrypt, hash) => {
                users.password = hash
                User.create(userData)
                .then(usuario => {
                    res.json({status: userData.email + ' registrado'})
                })
                .catch( err => {
                    res.send('error: ' + err)
                })
            })
        }else{
            res.json({error: "Usuario ya existente"})
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})

users.post('/login', (req, res) => {
    User.findOne({
      where: {
        email: req.body.email
      }
    })
      .then(usuario => {
        if (usuario) {
          if (req.body.password === usuario.password) {
            let token = jwt.sign(usuario.dataValues, process.env.SECRET_KEY, {
              expiresIn: 1440
            })
            res.send(token)
          }else{
            res.json({ error: 'Credenciales incorrectas' })
          }
        } else {
          res.json({ error: 'El usuario no existe' })
        }
      })
      .catch(err => {
        res.status(400).json({ error: err })
      })
  })

module.exports = users

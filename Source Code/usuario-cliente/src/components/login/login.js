import React, { useState } from 'react';
import './login.css';
import M from "materialize-css";
import postData from '../../utils/callServices';
import { useHistory } from "react-router-dom"
import {USER_LOGIN} from '../../constants/endPoints';


export default function Login() {

    const history = useHistory();

    const [datos, setDatos] = useState({
        email:'',
        password:''
    })

    const handleInputChange = (event) =>{
        setDatos({
            ...datos,
            [event.target.name] : event.target.value
        })
    }

    const loginUser =(event) =>{
        event.preventDefault();
        const qs = require('qs');
        const data = qs.stringify(datos);
        postData(USER_LOGIN,data).then(result =>{
            if(result.status!==null && result.error === undefined){
                M.toast({html: 'Credenciales correctas', classes: 'green rounded'})
                sessionStorage.setItem('_userToken', result);
                history.push("/userHome");
            }else if(result.error!==null){
                M.toast({html: result.error, classes: 'red rounded'})
            }	      
        });
        
    }

  return (
    <div className="container container-main">
        <div className="section">
            <div className="row">
                <div className="col s1 m3"></div>
                <div className="col s10 m6 white z-depth-5 mt-30 p-20">
                    <h5 className="center mt-0">Iniciar Sesión</h5>
                    <form className="row mb-0 login-form">
                        <div className="input-field col s12">
                            <input type="text" id="username" autoComplete="off" name="email" onChange={handleInputChange}/>
                            <label htmlFor="username">Correo electrónico</label>
                        </div>
                        <div className="input-field col s12">
                            <input type="password" id="password" autoComplete="off" name="password" onChange={handleInputChange}/>
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="row center">
                            <button className="btn waves-effect waves-light pink accent-4" onClick={loginUser}>Entrar</button>
                        </div>
                        <div className="row left mb-0 "></div>
                    </form>

                    <div className="col s1 m3"></div>
                </div>
            </div>
        </div>
    </div>
  );
}
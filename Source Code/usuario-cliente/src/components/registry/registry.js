import React, { useState } from 'react';
import './registry.css';
import M from "materialize-css";
import postData from '../../utils/callServices';
import { useHistory } from "react-router-dom";
import {USER_REGISTER} from '../../constants/endPoints';

export default function Registry() {

    const history = useHistory();

    const [datos, setDatos] = useState({
        name:'',
        surnames:'',
        email:'',
        password:''
    })
    
    const handleInputChange = (event) =>{
        setDatos({
            ...datos,
            [event.target.name] : event.target.value
        })
    }

    const saveUser = (event) =>{
        event.preventDefault();
        const qs = require('qs');
        const data = qs.stringify(datos);
        postData(USER_REGISTER,data).then(result =>{
            if(result.status!==null && result.error === undefined){
                M.toast({html: 'El registro fue exitoso', classes: 'green rounded'})
                history.push("/login");
            }else if(result.error!==null){
                M.toast({html: result.error, classes: 'red rounded'})
            }	      
        });
    }

  return (
    <div className="container container-main">
        <div className="section">
            <div className="row">
                <div className="col s1 m3"></div>
                <div className="col s10 m6 white z-depth-5 mt-30 p-20">
                    <h5 className="center mt-0">Registrar Usuario</h5>
                    <form className="row mb-0 login-form">
                        <div className="input-field col s12">
                            <input type="text" name="name" id="name" autoComplete="off" onChange={handleInputChange}/>
                            <label htmlFor="name">Nombre</label>
                        </div>
                        <div className="input-field col s12">
                            <input type="text" name="surnames" id="surnames" autoComplete="off" onChange={handleInputChange}/>
                            <label htmlFor="surnames">Apellidos</label>
                        </div>
                        <div className="input-field col s12">
                            <input type="email" name="email" id="email" autoComplete="off" onChange={handleInputChange}/>
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="input-field col s12">
                            <input type="password" name="password" id="password" autoComplete="off" onChange={handleInputChange}/>
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="row center">
                            <button className="btn waves-effect waves-light pink accent-4" onClick={saveUser} >Guardar</button>
                        </div>
                        <div className="row left mb-0 "></div>
                    </form>

                    <div className="col s1 m3"></div>
                </div>
            </div>
        </div>
    </div> 
  );
}
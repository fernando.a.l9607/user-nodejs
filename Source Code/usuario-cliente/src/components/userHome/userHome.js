import React from 'react';
import jwt_decode from 'jwt-decode'
import { Redirect } from "react-router-dom";



export default function UserHome() {


    const token = sessionStorage.getItem('_userToken');
    if(token === null){
        return <Redirect to="/login" />
    }
    const decoded = jwt_decode(token);


  return (
    <div className="container container-main">
        <div className="section">
            <div className="row">
                <div className="col s12  white z-depth-5">
                    <h3 className="center-align" >Bienvenido {decoded.name} {decoded.surnames}</h3>
                    <h5 className="center-align" >Tu correo electronico es: {decoded.email}</h5>
                    <h5 className="center-align" >Fecha de creacion: {decoded.produce}</h5>
                </div>
            </div>
        </div>
    </div>
  );
}
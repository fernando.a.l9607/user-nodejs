import axios from 'axios';

const header = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Origin':'*'
}

export default function postData(url,body){
    
    return axios.post(url,body,header)
    .then(response => {
        return JSON.parse(JSON.stringify(response.data));
    })
    .catch(error =>{
        return JSON.parse(JSON.stringify(error));
    });
}